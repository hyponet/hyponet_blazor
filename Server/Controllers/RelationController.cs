﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlazorAppHyponet.Shared;

namespace BlazorAppHyponet.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RelationController : ControllerBase
    {
        
        public RelationController()
        {
           
        }

        [HttpGet("[action]/{fromNodeId}/{toNodeId}/{relationType}")]
        public void Create(string fromNodeId, string toNodeId, string relationType)
        {
            HttpClientManager httpClientManager = new HttpClientManager("https://localhost", "44380");
            var endpointUri = $"/Relation/Create/{fromNodeId}/{toNodeId}/{relationType}";
            httpClientManager.SendHttpRequestMessage("GET", endpointUri);
        }

        [HttpGet("[action]/{fromNodeId}/{toNodeId}/{relationType}")]
        public List<string> Delete(string fromNodeId, string toNodeId, string relationType)
        {
            throw new NotImplementedException();
        }


    }
}
