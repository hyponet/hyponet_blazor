﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorAppHyponet.Shared;
using BlazorAppHyponet.Shared.Models;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Newtonsoft.Json;

namespace BlazorAppHyponet.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NodeController : ControllerBase
    {
        [HttpGet("[action]/{name}/{type}")]
        public List<Node> Create(string name, string type)
        {

            HttpClientManager httpClientManager = new HttpClientManager("https://localhost", "44380");
            var endpointUri = $"/Node/Create/{name}/{type}";
            var jsonListOfNodes= httpClientManager.SendHttpRequestMessage("GET", endpointUri);
            var nodeList = JsonConvert.DeserializeObject<List<Node>>(jsonListOfNodes);

            return nodeList;
        }

    }
}
