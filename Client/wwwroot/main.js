
//Hent det markerede
function getSelectedText() {
    return window.getSelection().toString();
}


//Indsæt Greenbox
function CreateGreenBox() {

    console.log("grønboks")
    var greenBox = document.createElement("Greenbox");

    document.getElementById("uddybninger").append(greenBox);
}

  




//Opret en taleboblehale der går fra uddybningstekstfeltet til den mark-opmærkningen som udløste (uddybnings)tekstfeltet
  function DrawSpeechBubble(domElement, greenBox, resultObject) {
    //Lav en omgivende ramme
    var svgFrame = document.createElement('div');
    //Definér det der skal indsættes
    var svgHtml = "<svg><polyline id='pilTilMark" + resultObject.node[0].nodeId +
      "' class='pil' points=''/> <polyline id='bundTilPil" + resultObject.node[0].nodeId +
      "' class='pil' points=''/> </svg>";

    //indsæt element i omgivende ramme
    svgFrame.innerHTML = svgHtml;
    //indsæt p-elementet fra LavEnUddybningsboks til denne svg
    svgFrame.appendChild(greenBox);

    //Hvis markering foregår i redbox
    if (domElement.parentElement == $("p.redbox")) {
      //console.log("Uddybningsboks udspringer fra grundnode");
      document.getElementsByClassName("greenbox").append(svgFrame);
    } else {
      $(svgFrame).insertAfter(domElement);
    }

    //Find markeringens x og y-koordinater
    var selection = document.getElementById(resultObject.node[0].nodeId).getBoundingClientRect();
    //Find uddybningsfeltets x og y-koordinator
    var specification = greenBox.getBoundingClientRect();
    var arrow = document.getElementById("pilTilMark" + resultObject.node[0].nodeId);
    var bottomArrow = document.getElementById("bundTilPil" + resultObject.node[0].nodeId);
    var coordinatesArrowLeft = parseInt(specification.width / 4) + "," + parseInt(21); //Først sæt: x,y for tekstboks overkant
    var coordinatesOfArrowTip = parseInt(selection.x + (selection.width / 2)) + "," + parseInt(-20); //Andet sæt: x,y for mark-tag
    var coordinatesArrowRight = parseInt(specification.width / 3) + "," + parseInt(21); //Tredje sæt: x,y for tekstboks overkant
    //var coordsBottom = 
    //var lengthSideBottom = Math.sqrt((Math.pow(parseInt(specification.width / 4), 2) - Math.pow(parseInt(specification.width / 3)), 2)) + (Math.pow(parseInt(21), 2) - Math.pow(parseInt(21), 2));
    //Placering af taleboblepilens to ben og spids
    arrow.setAttributeNS(null, "points", coordinatesArrowLeft + " " + coordinatesOfArrowTip + " " + coordinatesArrowRight + " " + coordinatesArrowLeft);
    var coordinatesBottomRight = parseInt((specification.width - 1.5) / 3) + "," + parseInt(21);
    bottomArrow.setAttributeNS(null, "points", coordinatesArrowLeft + " " + coordinatesBottomRight);
    bottomArrow.setAttributeNS(null, "style", "stroke:#90ee90;stroke-width:1.5;stroke-linecap:round");
    //console.log("Taleboblens pil har følgende koordinater:");
    //console.log(arrow.getAttributeNS(null, "points"));
  }





















 







  //Søg efter andre noder
  async function SearchNodes(searchChars) {

    var encodedString = encodeURIComponent(searchChars);

    var apiEndpointUrl = "https://localhost:44380/Node/SearchNodes/" + encodedString;
    var SearchResult = await httpGetAsync(apiEndpointUrl);
    return SearchResult;
  };



















  async function MarkNodeCreation(selectedText, domElement) {

    var selOffsets = getSelectionCharacterOffsetWithin(domElement)
    var start = selOffsets.start;
    var end = selOffsets.end;
    //console.log("Markering fra bogstav: " + start + " til bogstav " + end);

    //Resultobject indeholder: element og node
    var resultObject = await CreateMarkNode(selectedText, domElement);
    var selectionObject = SurroundSelectedTextWithMarkTag(resultObject);

    SetBoxId(selectionObject);
    //domelement er boksen den er lavet i, ROOT eller SPEC. 
    //selectionobject er markeringen
    await CreateRelation(domElement.id, selectionObject.node[0].nodeId, "Mark");


    await MergeMarkNodes(selectedText, domElement, start, end);


    CreateGreenBox(domElement, selectionObject);
  }












  //Send indhold til neo4j om at oprette en marknode
  async function CreateMarkNode(selectedText, domElement) {


    var nodeType = "MARK";
    var selOffsets = getSelectionCharacterOffsetWithin(domElement)
    var start = selOffsets.start;
    var end = selOffsets.end;

    var postChar = getCharacterSucceedingSelection(domElement)
    var preChar = getCharacterPrecedingSelection(domElement)

    //encode null as string to avoid 404 in httpGet
    if (postChar == "") {
      var postChar = " ";
    }
    if (preChar == "") {
      var preChar = " ";
    }
    var encodedString = encodeURIComponent($.trim(selectedText));
    var apiEndpointUrl = "https://localhost:44380/Node/Create/" + encodedString + "/" + nodeType + "/" + start + "/" + end + "/" + encodeURIComponent(preChar) + "/" + encodeURIComponent(postChar) + "/";


    var nodeResult = await httpGetAsync(apiEndpointUrl, domElement);

    return nodeResult;
  }











  async function MergeMarkNodes(selectedText, domElement, selectionStart, selectionEnd) {
    var nodeType = "MARK";
    var encodedString = encodeURIComponent($.trim(selectedText));
    var apiEndpointUrl = "https://localhost:44380/Node/MergeMarkNodes/" + encodedString + "/" + nodeType + "/" + selectionStart + "/" + selectionEnd + "/" + domElement.id;
    var nodeResult = await httpGetAsync(apiEndpointUrl, domElement);

    return nodeResult;
  }



  //Vi skal have fundet en måde at slette r i SPEC-[:Ass]->ASS-[r:Ass]->MARK fordi r er lavet automatisk 
  async function DeleteRelation(fromNodeId, toNodeId, relationType) {
    var apiEndpointUrl = "https://localhost:44380/Relation/Delete/" + fromNodeId + "/" + toNodeId + "/" + relationType;
    var nodeResult = await httpGetAsync(apiEndpointUrl);

    return nodeResult;
  }

  async function SpecNodeCreation(e, markNodeOrigin) {
    var resultObject = await CreateSpecNode(e.currentTarget);
    SetBoxId(resultObject);
    await CreateRelation(markNodeOrigin.node[0].nodeId, resultObject.node[0].nodeId, "Spec");
    //Find ASS-noder (fromNode) at forbinde til denne SPEC (toNode)        
    var FromASSToSPEC = await FindAssToRelateTo(resultObject.node);
    if (FromASSToSPEC.node.fromNodeID != null || FromASSToSPEC.node.toNodeID != null) {
      await CreateRelation(FromASSToSPEC.node.fromNodeID.toString(), FromASSToSPEC.node.toNodeID.toString(), "Ass");
    }

    await SeekOutput(resultObject);
    return resultObject;
  }

  async function FindAssToRelateTo(fromNode) {

    var apiEndpointUrl = "https://localhost:44380/Node/FindAssToRelateTo/" + fromNode[0].nodeId;
    var nodeResult = await httpGetAsync(apiEndpointUrl);

    return nodeResult;
  }

  //Send indhold til neo4j om at oprette en Spec-node
  async function CreateSpecNode(greenBoxElement) {
    var nodeType = "SPEC";
    var encodedString = encodeURIComponent($.trim(greenBoxElement.innerText));
    var apiEndpointUrl = "https://localhost:44380/Node/Create/" + encodedString + "/" + nodeType;
    var nodeResult = await httpGetAsync(apiEndpointUrl, greenBoxElement);
    return nodeResult;
  }


  async function SeekOutput(SpecNode) {

    var apiEndpointUrl = "https://localhost:44380/Node/SeekOutput/" + SpecNode.node[0].nodeId;

    var hvad = await httpGetAsync(apiEndpointUrl)
    console.log("der blev fundet noget")
    console.log(hvad)


  };


  async function AssNodeCreation(fromResultObject, markResultObject) {
    //opret en association i databasen baseret på markeringen
    var resultObject = await CreateAssNode(fromResultObject.element, markResultObject.element.innerText);
    //opret en relation i databaen baseret på hvilken SPEC den er lavet i og hvilken MARK den er lavet i
    await CreateRelation(fromResultObject.node[0].nodeId, resultObject.node[0].nodeId, "Ass");

    //Vælg hvilken ASS-node fra databasen der skal dukke op
    var SPECnodeASSnode = await ChooseAssNode(fromResultObject)

    if (SPECnodeASSnode.node[0]) {
      CreateOutputBox(SPECnodeASSnode)
    }

  }

  //Lav en Associationsnode
  async function CreateAssNode(fromElement, selectedText) {
    var nodeType = "ASS";
    var encodedString = encodeURIComponent($.trim(selectedText));
    var apiEndpointUrl = "https://localhost:44380/Node/Create/" + encodedString + "/" + nodeType;

    var resultObject = await httpGetAsync(apiEndpointUrl, fromElement)
    console.log(nodeType + selectedText)


    return resultObject;
  };

  //Find ASS-node med flest veje til sig
  async function ChooseAssNode(domElement) {


    var apiEndpointUrl = "https://localhost:44380/Node/ChooseAssNode/" + domElement.node[0].nodeId;


    var nodeResult = await httpGetAsync(apiEndpointUrl, domElement)

    return nodeResult;
  };



  //fromNodeId når der skal laves ASS pga en uddybning er næsten altid SPEC og selvfølgelig er toNodeId en ASS og typen er Ass

  async function CreateRelation(fromNodeId, toNodeId, relationType) {
    var apiEndpointUrl = "https://localhost:44380/Relation/Create/" + fromNodeId + "/" + toNodeId + "/" + relationType;
    var nodeResult = await httpGetAsync(apiEndpointUrl);

    return nodeResult;
  }


  //Hent ID fra neo4j til den samme tekstfelt som netop er oprettet i UI
  function SetBoxId(resultObject) {
    //console.log(resultObject.element);
    $(resultObject.element).attr("id", resultObject.node[0].nodeId)
  }












  //Åbner ajax-api-xmlhttprequest-xhr
  function httpGetAsync(theUrl, htmlElement) {
    return new Promise(function (resolve, reject) {
      var xmlHttp = new XMLHttpRequest();
      var element = htmlElement;
      xmlHttp.onreadystatechange = async function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
          console.log("readystate = 4: " + xmlHttp.responseText);
          var jsonResult = await JSON.parse(xmlHttp.responseText);
          var resultObject = {
            node: jsonResult,
            element: element
          };
          resolve(resultObject);
          return resultObject;
        }
        if (xmlHttp.readyState == 3) {
          //console.log(xmlHttp.responseText);
        } else {
          console.log("rejected at readystate = " + xmlHttp.readyState);
          //reject("REJECT");
        }
      }
      xmlHttp.open("GET", theUrl, true); // true for asynchronous
      xmlHttp.send();

    })
  }


  //Send grundnode afsted
  SendGrundNode()





  //Få start- og slutplacering af markering uanset om der er anden html-opmærkning i feltet
  function getSelectionCharacterOffsetWithin(element) {
    var start = 0;
    var end = 0;
    var doc = element.ownerDocument || element.document;
    var win = doc.defaultView || doc.parentWindow;
    var sel;

    if (typeof win.getSelection != "undefined") {
      sel = win.getSelection();
      if (sel.rangeCount > 0) {
        var range = win.getSelection().getRangeAt(0);
        var preCaretRange = range.cloneRange();
        preCaretRange.selectNodeContents(element);
        preCaretRange.setEnd(range.startContainer, range.startOffset);
        start = preCaretRange.toString().length;
        preCaretRange.setEnd(range.endContainer, range.endOffset);
        end = preCaretRange.toString().length;
      }
    } else if ((sel = doc.selection) && sel.type != "Control") {
      var textRange = sel.createRange();
      var preCaretTextRange = doc.body.createTextRange();
      preCaretTextRange.moveToElementText(element);
      preCaretTextRange.setEndPoint("EndToStart", textRange);
      start = preCaretTextRange.text.length;
      preCaretTextRange.setEndPoint("EndToEnd", textRange);
      end = preCaretTextRange.text.length;
    }
    return {
      start: start,
      end: end
    };

  }



  //Find bogstav før markering
  function getCharacterPrecedingSelection(containerEl) {
    var precedingChar = "",
      sel, range, precedingRange;
    if (window.getSelection) {
      sel = window.getSelection();
      if (sel.rangeCount > 0) {
        range = sel.getRangeAt(0).cloneRange();
        range.collapse(true);
        range.setStart(containerEl, 0);
        precedingChar = range.toString().slice(-1);
      }
    } else if ((sel = document.selection) && sel.type != "Control") {
      range = sel.createRange();
      precedingRange = range.duplicate();
      precedingRange.moveToElementText(containerEl);
      precedingRange.setEndPoint("EndToStart", range);
      precedingChar = precedingRange.text.slice(-1);
    }
    return precedingChar;
  }


  //Find bogstav efter markering
  function getCharacterSucceedingSelection(containerEl) {
    var succeedingChar = "",
      sel, range, succeedingRange;
    if (window.getSelection) {
      sel = window.getSelection();
      if (sel.rangeCount > 0) {
        range = sel.getRangeAt(0).cloneRange();
        range.collapse(false);
        range.setEnd(containerEl, containerEl.childNodes.length);
        succeedingChar = range.toString().charAt(0);
      }
    } else if ((sel = document.selection) && sel.type != "Control") {
      range = sel.createRange();
      succeedingRange = range.duplicate();
      succeedingRange.moveToElementText(containerEl);
      succeedingRange.setEndPoint("EndToStart", range);
      succeedingChar = succeedingRange.text.slice(-1);
    }
    return succeedingChar;
  }





  //Marker tekst i tekstfeltet
  function SelectTextFromWindow(event) {
    var selectedText = window.getSelection();
    var domElement = event.target;

    if (selectedText.type != "Caret") {

      //Gem teksten før og efter markeringen (skal bruges til at notere morfemgrænser i MARK-noden)
      var precedingChar = getCharacterPrecedingSelection(domElement);
      var succeedingChar = getCharacterSucceedingSelection(domElement)
      console.log("Før denne markering: " + precedingChar);
      console.log("Efter denne markering: " + succeedingChar);

      //udfør kun hvis der ER markeret noget OG der ikke blot er markeret mellemrum eller et punktum
      if ((selectedText.toString().trim() != '') && (selectedText.toString().trim() != '.')) {

        //Udvid det valgte indtil næste whitespace/mellemrum eller specialtegn    
        //snapSelectionToWord(selectedText)
        var ord = getSurroundingWords(selectedText)
        console.log(ord)

        //Send indholdet, hvis man har glemt at trykke ENTER
        MarkNodeCreation(selectedText, domElement, precedingChar, succeedingChar)
      };

    }



    return selectedText, domElement;
  }


  //Denne funktion skal markere ordet foran selection og bagefter selection.
  function getSurroundingWords(selectedText) {

    console.log(selectedText)




    //________________________TILGANG 1
    //// get the selection
    //var sel = window.getSelection();
    //// get the element in which the selection is made, and the start and end position
    //var node = sel.anchorNode;
    //var from = sel.anchorOffset;
    //// let's see what's before the selection
    //var textBeforeWord = node.textContent.substring(0, from);
    //var match = textBeforeWord.match(/(\w+)\s+/);
    //var previousWord = match[1];



    //________________________TILGANG 2
    ////spring over ordet og marker hele næste
    //selectedText.modify("move", "forward", "word");
    //selectedText.modify("extend", "forward", "word");
    //var nextWord = selectedText.toString();
    //console.log(nextWord)
    ////hop tilbage til ordet og marker hele forrige 
    //selectedText.modify("move", "backward", "word");
    //selectedText.modify("move", "backward", "word");
    //selectedText.modify("extend", "backward", "word");
    //var previousWord = selectedText.toString();
    //console.log(previousWord)
    ////hop frem igen og mark
    //selectedText.modify("move", "forward", "word");
    //selectedText.modify("extend", "forward", "word");




    //__________________________TILGANG 3
    var value = $(this).text();
    var res = value.split(" ");

    var name = res[2];
    var title = res[3];

    var html = "<div class=\"showLeft\">" + name + "</div> " + " <div class=\"showLeft\">" + title + "</div>";

    $(this).html(html);

    $(".showLeft").on('click', function () {
      alert($(this).prev().text());
    })




  }

  //Denne funktion skal forlænge markeringer til nærmeste whitespace hvis der er under 1 characters tilbare af ordet
  function snapSelectionToWord(selectedText) {

    var sel

    // Check for existence of window.getSelection() and that it has a
    // modify() method. IE 9 has both selection APIs but no modify() method.
    if (selectedText && (sel = window.getSelection()).modify) {
      sel = selectedText;
      if (!sel.isCollapsed) {

        // Detect if selection is backwards
        var range = document.createRange();
        range.setStart(sel.anchorNode, sel.anchorOffset);
        range.setEnd(sel.focusNode, sel.focusOffset);
        var backwards = range.collapsed;
        range.detach();

        // modify() works on the focus of the selection
        var endNode = sel.focusNode,
          endOffset = sel.focusOffset;
        //udvid kun til sel.focusNode.nodeValue = Mark.name
        sel.collapse(sel.anchorNode, sel.anchorOffset);
        if (backwards) {
          sel.modify("move", "backward", "character");
          sel.modify("move", "forward", "word");
          sel.extend(endNode, endOffset);
          sel.modify("extend", "forward", "character");
          sel.modify("extend", "backward", "word");
          sel.trim()

        } else {
          sel.modify("move", "forward", "character");
          sel.modify("move", "backward", "word");
          sel.extend(endNode, endOffset);
          sel.modify("extend", "backward", "character");
          sel.modify("extend", "forward", "word");

        }
      }
    } else if ((sel = document.selection) && sel.type != "Control") {
      var textRange = sel.createRange();
      if (textRange.text) {
        textRange.expand("word");
        // Move the end back to not include the word's trailing space(s),
        // if necessary
        while (/\s$/.test(textRange.text)) {
          textRange.moveEnd("character", -1);
        }
        textRange.select();
      }
    }


  }








  //Opret gule mark-opmærkninger
  // OG
  //Hent ID fra neo4j til den samme mark-opmærkning som netop er oprettet
  function SurroundSelectedTextWithMarkTag(resultObject) {
    //marknodens ID - egenskab fra objekt
    //console.table([resultObject.node]);

    var selection = window.getSelection();
    var selectionRange = selection.getRangeAt(0);
    var newMarkElement = document.createElement("mark");
    resultObject.element = newMarkElement;
    selectionRange.surroundContents(newMarkElement);
    //console.log("markering på: " + selectionRange.toString());
    return resultObject;
  }


  //Fjerner mark-tag fra uddybning, så man kan lave en overlappende markering
  //(det er en hack. Jeg vil foretrække at man kan markere flere gange oveni hinanden)
  function FjernMarkTag(event) {


    if (event.currentTarget.childElementCount > 0) {
      if (event.target.matches('mark')) {

        let teksten = event.currentTarget.textContent;
        let thismark = event.target;
        thismark.remove();
        event.currentTarget.innerText = teksten
      } else {
        let marktag = document.getElementById(event.target.id).getElementsByTagName("mark");
        while (marktag.length) {
          let parent = marktag[0].parentNode;
          while (marktag[0].firstChild) {
            parent.insertBefore(marktag[0].firstChild, marktag[0]);
          }
          //console.log("Der fjernes " + event.currentTarget.childElementCount + " markeringer i dette felt")

          parent.removeChild(marktag[0]);

        }
      }


    } else {
      //console.log("Der er ingen mark-tag i denne node");
    }

  }


