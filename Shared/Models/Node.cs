﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BlazorAppHyponet.Shared.Models
{
    public class Node
    {
        [JsonProperty("creationTime")] 
        public string CreationTime { get; set; }

        [JsonProperty("nodeName")] 
        public string NodeName { get; set; }

        [JsonProperty("nodeId")] 
        public string NodeId { get; set; }

        [JsonProperty("nodeLabel")] 
        public string NodeLabel { get; set; }
    }
}