﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace BlazorAppHyponet.Shared
{
    public class HttpClientManager
    {
        public String Hostname { get; set; }
        public String PortNumber { get; set; }

        public Uri BaseAddress
        {
            get => new Uri(Hostname + ":" + PortNumber);
        }

        public HttpClient HttpClient { get; set;}

        public HttpClientManager(string hostname, string portNumber)
        {
            Hostname = hostname;
            PortNumber = portNumber;
            HttpClient = GetHttpClient();
        }

        public HttpClient GetHttpClient()
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = BaseAddress;
            return httpClient;
        }

        public string SendHttpRequestMessage(string method, string endpointUri)
        {
            var httpRequestMessage = new HttpRequestMessage(new HttpMethod(method), endpointUri);
            var httpResponseMessage = HttpClient.SendAsync(httpRequestMessage).Result;
            var responseContent = httpResponseMessage.Content.ReadAsStringAsync().Result;
            return responseContent;

        }

    }
}
